# Mishal Alajmi
# 15/07/2020
# Contact: [Gitlab: Mishal Alajmi]
#          [Github: Mishal-Alajmi]
#          [email: mishal.ajm@gmail.com]
# Bubble sort implementation


def bubble_sort(unsorted_list)
  if unsorted_list.nil?
    return nil
  end
  puts "List before sorting #{unsorted_list}"
  loop do 
    swapped = false
    (unsorted_list.length-1).times do |elm|
      if unsorted_list[elm] > unsorted_list[elm + 1]
        unsorted_list[elm], unsorted_list[elm + 1] = unsorted_list[elm + 1], unsorted_list[elm]
        swapped = true
        puts "Current ordering #{unsorted_list}"
      end
    end
    break if not swapped
  end
end

bubble_sort([4,3,78,2,0,2])
